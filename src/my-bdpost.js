//----------------------------------------------------------------------------------------------------------------
// Recupera la información de un post concreto
//----------------------------------------------------------------------------------------------------------------

  function obtenerPost() {

    var idPost = sessionStorage.getItem("sessionIdPost");
    var urlGet = "https://api.mlab.com/api/1/databases/blog/collections/post/" + idPost + "?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"

     console.log(urlGet);

     var peticion = new XMLHttpRequest();
     peticion.open("GET", urlGet, false);
     peticion.setRequestHeader("Content-Type", "application/json");
     peticion.send();

     var respuesta = JSON.parse(peticion.responseText);
     console.log(respuesta);

     if (peticion.status=="200")
     {
        console.log("respuesta");
       return respuesta;
     }
     else
     {
        alert("get incorrecto");
     }

  }

  //----------------------------------------------------------------------------------------------------------------
  // Alta de un post
  //----------------------------------------------------------------------------------------------------------------

  function incluirPost () {

  //MLAB BBDD - Blog coleccion - post
      const URLMLABPOST = "https://api.mlab.com/api/1/databases/blog/collections/post?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe";

      var ok = true;
      var altaCity = document.getElementsByName("city")[0].value;
      var altaCountry = document.getElementsByName("country")[0].value;
      var altaTitule = document.getElementsByName("titulo")[0].value;
      var altaPost = document.getElementsByName("post")[0].value;
      var altaUser = sessionStorage.getItem("sessionUser");
      var altaImagen ="/images/fotodetalle.jpg";

      var d = new Date();
      var altaDate =d.toUTCString();
      var altaLocation = '"location":{"city":"'+altaCity+'", "country":"'+altaCountry+'"}';
      var newPost = '{'+altaLocation+',"titulo":"'+altaTitule+'","post":"'+altaPost+'","imagen":"'+altaImagen+'","user":"'+altaUser+'","date":"'+altaDate+'"}';
      console.log(newPost);


  // Alta en BBDD post de la aplicacion


      var peticion = new XMLHttpRequest();
      peticion.open("POST", URLMLABPOST, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send(newPost);

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);

      if (peticion.status != "200"){
          alert("alta fallida");
          ok = false;
      }
      return ok;
  }


  //----------------------------------------------------------------------------------------------------------------
  // Modificar la información de un post
  //----------------------------------------------------------------------------------------------------------------

  function modificarPost () {

  //MLAB BBDD - Blog coleccion - post

      var ok = true;

      var updateCity = document.getElementsByName("city")[0].value;
      var updateCountry = document.getElementsByName("country")[0].value;
      var updateTitule = document.getElementsByName("titulo")[0].value;
      var updatePost = document.getElementsByName("post")[0].value;

      var updateUser = sessionStorage.getItem("sessionUser");

      var d = new Date();
      var updateDate =d.toUTCString();

      var updateLocation = '"location":{"city":"'+updateCity+'", "country":"'+updateCountry+'"}';
      var updateImagen = "/images/fotodetalle.jpg";

  // Montamos URL de invocacción al update en BBDD post de la aplicacion

      var idPost = sessionStorage.getItem("sessionIdPost");

      console.log(idPost);

      var urlPut = "https://api.mlab.com/api/1/databases/blog/collections/post/" + idPost + "?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"
      console.log(urlPut);

      var updatepost = '{'+updateLocation+',"titulo":"'+updateTitule+'","post":"'+updatePost+'","imagen":"'+updateImagen+'","user":"'+updateUser+'","date":"'+updateDate+'"}';
      console.log(updatepost);

      var peticion = new XMLHttpRequest();
      peticion.open("PUT", urlPut, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send(updatepost);

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);

      if (peticion.status != "200"){
          alert("update fallida");
          ok = false;
      }
      return ok;
  }


  //----------------------------------------------------------------------------------------------------------------
  // Borrar un post
  //----------------------------------------------------------------------------------------------------------------

  function borrarPost () {
      //MLAB BBDD - Blog coleccion - post

      var ok = true;
      var idPost = sessionStorage.getItem("sessionIdPost");
      var urlDel = "https://api.mlab.com/api/1/databases/blog/collections/post/" + idPost + "?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"
      console.log(urlDel);

      var peticion = new XMLHttpRequest();
      peticion.open("DELETE", urlDel, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);

      if (peticion.status != "200"){
          alert("borrado fallido");
          ok = false;
      }
      return ok;
  }

 //----------------------------------------------------------------------------------------------------------------
 // Recupera la lista de post de un usuario
 //----------------------------------------------------------------------------------------------------------------

   function obtenerMyPostList() {

      var pagActual = sessionStorage.getItem("sessionPagActual");
      var sessionUser = sessionStorage.getItem("sessionUser");
      var bloques = (pagActual-1)*3;
      console.log (sessionUser);
      console.log (pagActual);
      console.log (bloques);

      var criterio = "q={'user':'" + sessionUser + "'}&s={'date':1}&sk=" + bloques + "&l=3";

      var urlMyList = "https://api.mlab.com/api/1/databases/blog/collections/post?" + criterio + "&apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe";
      console.log(urlMyList);

      var peticion = new XMLHttpRequest();
      peticion.open("GET", urlMyList, false);
      peticion.setRequestHeader("Content-Type", "application/json");
      peticion.send();

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);

      if (peticion.status=="200")
      {
        return respuesta;
      }
      else
      {
         alert("get incorrecto");
      }

   }

   //----------------------------------------------------------------------------------------------------------------
   // Recupera el numero de registros
   //----------------------------------------------------------------------------------------------------------------

     function countMyPostList() {

        var sessionUser = sessionStorage.getItem("sessionUser");
        var criterio = "q={'user':'" + sessionUser + "'}&c=true";

        var urlMyList = "https://api.mlab.com/api/1/databases/blog/collections/post?" + criterio + "&apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe";
        console.log(urlMyList);

        var peticion = new XMLHttpRequest();
        peticion.open("GET", urlMyList, false);
        peticion.setRequestHeader("Content-Type", "application/json");
        peticion.send();

        var respuesta = JSON.parse(peticion.responseText);
        console.log(respuesta);

        if (peticion.status=="200")  {
              return respuesta;
        }
        else
        {
           alert("get incorrecto");
        }

     }
     //----------------------------------------------------------------------------------------------------------------
     // Recupera la lista de post de un usuario
     //----------------------------------------------------------------------------------------------------------------

       function obtenerUltPostList() {

          var pagActual = sessionStorage.getItem("sessionPagActual");

          var bloques = (pagActual-1)*3;
          console.log (pagActual);
          console.log (bloques);

          var criterio = "s={'date':1}&sk=" + bloques + "&l=3";

          var urlMyList = "https://api.mlab.com/api/1/databases/blog/collections/post?" + criterio + "&apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe";
          console.log(urlMyList);

          var peticion = new XMLHttpRequest();
          peticion.open("GET", urlMyList, false);
          peticion.setRequestHeader("Content-Type", "application/json");
          peticion.send();

          var respuesta = JSON.parse(peticion.responseText);
          console.log(respuesta);

          if (peticion.status=="200")
          {
            return respuesta;
          }
          else
          {
             alert("get incorrecto");
          }

       }
