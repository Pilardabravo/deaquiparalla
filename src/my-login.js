// Se valida el usuario mediante la API proporcionada por Angel

function validar () {

    document.getElementsByName("acceso")[0].href="/login"
    document.getElementsByName("mensaje")[0].style.display = "none";

    const URL = "http://postgrebc9688.cloudapp.net:3000/api/login";
    var todo_correcto = "true";
    const msg = "Usuario o clave incorrecto";

    var user = document.getElementsByName("nombre")[0].value;
    var password = document.getElementsByName("password")[0].value;

    if (user != "" && password != "") {
        console.log(user);
        console.log(password);

        var peticion = new XMLHttpRequest();
        peticion.open("POST", URL, false);
        peticion.setRequestHeader("Content-Type", "application/json");
        peticion.setRequestHeader("Access-Control-Allow-Origin", "*");
        peticion.send('{"nombre":"' + user + '", "password": "' + password + '"}');

        var respuesta = JSON.parse(peticion.responseText);
        if (peticion.status=="200" && respuesta.data.count > 0)
        {
            var guardar = guardarSessionUser(user);

            if (guardar == true) {
                initList();
                document.getElementsByName("acceso")[0].href="/home";

            }
            else {
              document.getElementsByName("mensaje")[0].value = msg;
              document.getElementsByName("mensaje")[0].style.display = "block";

            }
        }
        else
        {
            document.getElementsByName("mensaje")[0].value = msg;
            document.getElementsByName("mensaje")[0].style.display = "block";
            todo_correcto = false;
        }
    } else {

      document.getElementsByName("mensaje")[0].value = msg;
      document.getElementsByName("mensaje")[0].style.display = "block";
      todo_correcto = false;
    }
}


// Validado el usuario, recuperamos y guardamos sus datos en sesión

function guardarSessionUser(usuario) {

     var ok = true;


     var urlGet = "https://api.mlab.com/api/1/databases/blog/collections/users?q={'user':'" + usuario + "'}&apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"
     console.log(urlGet);

     var peticion = new XMLHttpRequest();
     peticion.open("GET", urlGet, false);
     peticion.setRequestHeader("Content-Type", "application/json");
     peticion.send();

     var respuesta = JSON.parse(peticion.responseText);
     console.log(respuesta[0]);

     var logUser = respuesta[0].user;
     var logEmail = respuesta[0].email;
     var logName = respuesta[0].nombre.name;
     var logSurname = respuesta[0].nombre.surname;
     var logBirth = respuesta[0].Nacimiento.birth;
     var logCountry = respuesta[0].Nacimiento.country;

     if (peticion.status=="200") {

         sessionStorage.setItem("sessionUser", logUser);
         sessionStorage.setItem("sessionName", logName);
         sessionStorage.setItem("sessionSurname", logSurname);
     }
     else  {
        ok = false;
    }

    return ok;
   }
