function salir () {

  sessionStorage.removeItem('sessionIdPost');
  sessionStorage.removeItem('sessionName');
  sessionStorage.removeItem('sessionSurname');
  sessionStorage.removeItem('sessionUser');
  sessionStorage.removeItem('sessionOpcionPost');
  sessionStorage.removeItem('sessionPagActual');
  sessionStorage.removeItem('sessionNumPage');
  sessionStorage.removeItem('sessionPost1');
  sessionStorage.removeItem('sessionPost2');
  sessionStorage.removeItem('sessionPost3');

  this.session = null;
  alert("¡Hasta pronto!");

}

function initList() {

  sessionStorage.setItem("sessionPagActual", "1");
  var numDocument = countMyPostList();
  var numPage=parseInt(numDocument)/3;

  sessionStorage.setItem("sessionNumPage", numPage);
  
}
function initUltList() {
  sessionStorage.setItem("sessionPagActual", "1");
}
